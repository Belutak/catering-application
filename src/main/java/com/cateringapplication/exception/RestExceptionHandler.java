package com.cateringapplication.exception;

import com.cateringapplication.exception.customer.CustomerErrorResponse;
import com.cateringapplication.exception.customer.CustomerNotFoundException;
import com.cateringapplication.exception.meal.MealErrorResponse;
import com.cateringapplication.exception.meal.MealNotFoundException;
import com.cateringapplication.exception.order.OrderErrorResponse;
import com.cateringapplication.exception.order.OrderNotFoundException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<CustomerErrorResponse> handleException(CustomerNotFoundException exc) {

        CustomerErrorResponse error = new CustomerErrorResponse(HttpStatus.NOT_FOUND.value(),
                exc.getMessage(),
                System.currentTimeMillis());

        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<CustomerErrorResponse> handleException(Exception exc) {

        CustomerErrorResponse error = new CustomerErrorResponse(HttpStatus.BAD_REQUEST.value(),
                exc.getMessage(),
                System.currentTimeMillis());

        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<MealErrorResponse> handleException(MealNotFoundException exc) {

        MealErrorResponse error = new MealErrorResponse(HttpStatus.NOT_FOUND.value(),
                exc.getMessage(),
                System.currentTimeMillis());

        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

//    @ExceptionHandler
//    public ResponseEntity<MealErrorResponse> handleException(Exception exc) {
//
//        MealErrorResponse error = new MealErrorResponse(HttpStatus.BAD_REQUEST.value(),
//                exc.getMessage(),
//                System.currentTimeMillis());
//        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
//    }

    @ExceptionHandler
    public ResponseEntity<OrderErrorResponse> handleException(OrderNotFoundException exc) {

        OrderErrorResponse error = new OrderErrorResponse(HttpStatus.NOT_FOUND.value(),
                exc.getMessage(),
                System.currentTimeMillis());

        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    //does nothing
//    @ExceptionHandler(MethodArgumentNotValidException.class)
//    public ResponseEntity<ValidatorErrorResponse> handleException(MethodArgumentNotValidException e) {
//
//        ValidatorErrorResponse error = new ValidatorErrorResponse(HttpStatus.BAD_REQUEST.value(),
//                e.getMessage(),
//                System.currentTimeMillis());
//
//        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
//    }

    @ExceptionHandler({DataIntegrityViolationException.class})
    public ResponseEntity<Object> handleBadRequest(final DataIntegrityViolationException ex, final WebRequest request) {
        final String bodyOfResponse = "Bad request!";
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

}
