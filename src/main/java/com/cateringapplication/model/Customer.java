package com.cateringapplication.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="customer")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="customerId")
    @JsonIgnore
    private long id;

    @Column(name="firstName")
    private String firstName;

    @Column(name = "lastName")
    private String lastName;

    @Column(name="email")
    @Email
    private String email;

    @OneToMany(mappedBy = "customer",
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    @JsonBackReference
    //@JsonManagedReference
    private List<CustomersMeals> customerMealList;

    public Customer() {
    }

    public Customer(String firstName, String lastName, @Email String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public Customer(String firstName, String lastName, @Email String email, List<CustomersMeals> customerMealList) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.customerMealList = customerMealList;
    }



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<CustomersMeals> getCustomerMealList() {
        return customerMealList;
    }

    public void setCustomerMealList(List<CustomersMeals> customerMealList) {
        this.customerMealList = customerMealList;
    }

    public void addCustomerMeal(CustomersMeals customerMeal) {

        if (customerMealList == null) {
            customerMealList = new ArrayList<>();
        }

        customerMealList.add(customerMeal);
    }
}
