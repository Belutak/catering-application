package com.cateringapplication.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="customersMeals")
public class CustomersMeals {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "customerID")
    @JsonManagedReference
    private Customer customer;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "mealID")
    @JsonManagedReference
    private Meal meal;

    @Temporal(TemporalType.DATE)
    @Column(name="creationDate")
    private Date creationDate;


    public CustomersMeals() {
    }

    public CustomersMeals(Customer customer, Meal meal, Date creationDate) {
        this.customer = customer;
        this.meal = meal;
        this.creationDate = creationDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Meal getMeal() {
        return meal;
    }

    public void setMeal(Meal meal) {
        this.meal = meal;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }



}
