package com.cateringapplication.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="meal")
public class Meal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="mealId")
    @JsonIgnore
    private long id;

    @Column(name = "mealName")
    private String mealName;

    @OneToMany(mappedBy = "meal",
            cascade = CascadeType.ALL,
            fetch = FetchType.EAGER)
    @JsonBackReference
   // @JsonManagedReference
    private List<CustomersMeals> mealCustomerList;

    public Meal() {
    }

    public Meal(String mealName) {
        this.mealName = mealName;
    }

    public Meal(String mealName, List<CustomersMeals> mealCustomerList) {
        this.mealName = mealName;
        this.mealCustomerList = mealCustomerList;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }


}
