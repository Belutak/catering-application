package com.cateringapplication.repository;

import com.cateringapplication.model.CustomersMeals;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface CustomersMealsOrderRepository extends CrudRepository<CustomersMeals, Long> {

    public List<CustomersMeals> findAllByCreationDate(Date date);
}
