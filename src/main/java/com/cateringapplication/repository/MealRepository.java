package com.cateringapplication.repository;

import com.cateringapplication.model.Meal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MealRepository extends CrudRepository<Meal, Long> {

    public Meal findByMealName(String mealName);
}
