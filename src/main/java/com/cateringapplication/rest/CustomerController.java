package com.cateringapplication.rest;

import com.cateringapplication.exception.customer.CustomerNotFoundException;
import com.cateringapplication.model.CustomersMeals;
import com.cateringapplication.rest.dto.customer.CustomerDTO;
import com.cateringapplication.service.impl.CustomerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Email;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerServiceImpl customerService;

    @PostMapping("/create")
    private ResponseEntity newCustomer(@Valid @RequestBody CustomerDTO customerDTO) {

        return ResponseEntity.ok(customerService.createCustomer(customerDTO));
    }

    @GetMapping("/getall")
    public ResponseEntity getCustomers() throws CustomerNotFoundException {

        return ResponseEntity.ok(customerService.getAllCustomers());
    }

    @GetMapping("/get/")
    public ResponseEntity getOneCustomer(@RequestParam(required = false, name = "email") String email) throws CustomerNotFoundException {

        return ResponseEntity.ok(customerService.getOneCustomerByEmail(email));
    }

    @PutMapping("/update")
    public ResponseEntity updateCustomer(@Valid @RequestBody CustomerDTO customerDTO,
                                         @RequestParam(required = false, name = "email") String email)
            throws CustomerNotFoundException {

        return ResponseEntity.ok(customerService.updateCustomerByEmail(customerDTO, email));

    }

    @DeleteMapping("/delete")
    public void deleteCustomer(@RequestParam(required = false, name = "email") String email) throws CustomerNotFoundException {

        customerService.deleteCustomerByEmail(email);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteCustomer(@PathVariable long id) throws CustomerNotFoundException {

        customerService.deleteCustomerById(id);
    }


    @PostMapping("order")
    public CustomersMeals orderTest(@Valid @RequestBody CustomerDTO customerDTO,
                                    @RequestParam (required = false, name= "mealname") String mealName) {


        return customerService.orderTesting(customerDTO, mealName);
    }

}
