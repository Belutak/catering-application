package com.cateringapplication.rest;

import com.cateringapplication.exception.order.OrderNotFoundException;
import com.cateringapplication.model.CustomersMeals;
import com.cateringapplication.rest.dto.customer.CustomerDTO;
import com.cateringapplication.rest.dto.customersmealsorder.CustomersMealsOrderDTO;
import com.cateringapplication.service.CustomersMealsOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("/order")
public class CustomersMealsOrderController {

    @Autowired
    private CustomersMealsOrderService customersMealsOrderService;


    //get orders for a specific date
    @GetMapping("/getbydate")
    public ResponseEntity<List<CustomersMealsOrderDTO>> getOrdersByDate(@RequestParam(required = false, name = "date") String date) throws ParseException, OrderNotFoundException {

        return ResponseEntity.ok(customersMealsOrderService.getAllOrdersByDate(date));
    }

    @PostMapping("/neworder/")
    public ResponseEntity<CustomersMealsOrderDTO> createNewOrder(@Valid @RequestBody CustomerDTO customerDTO, @RequestParam (required = false, name="mealname") String mealName) {

        return ResponseEntity.ok(customersMealsOrderService.createOrder(customerDTO, mealName));
    }

    //get all orders
    @GetMapping("/getall")
    public ResponseEntity<List<CustomersMealsOrderDTO>> getAllOrder() {

        return ResponseEntity.ok(customersMealsOrderService.findAllOrders());
    }

    //testing
    @GetMapping("/getbyemail/")
    public List<CustomersMeals> getByCMEmail(@RequestParam(required = false, name="email") String email) {

        return customersMealsOrderService.findByCustomerEmail(email);
    }
}
