package com.cateringapplication.rest;

import com.cateringapplication.exception.meal.MealNotFoundException;
import com.cateringapplication.rest.dto.meal.MealDTO;
import com.cateringapplication.service.impl.MealServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/meal")
public class MealController {

    @Autowired
    private MealServiceImpl mealService;

    @PostMapping("/create")
    private void newmeal(@RequestBody MealDTO mealDTO) {

        mealService.createMeal(mealDTO);
    }

    @GetMapping("/getall")
    public ResponseEntity getMeals() {

        return  ResponseEntity.ok(mealService.getAllMeals());
    }

    @GetMapping("/get/")
    public ResponseEntity getOneMeal(@RequestParam(required = false, name = "mealname") String mealName ) {

        return ResponseEntity.ok(mealService.getOneMealByMealName(mealName));
    }

    @PutMapping("/update")
    public ResponseEntity updateMeal(@RequestBody MealDTO mealDTO,
                                         @RequestParam(required = false, name = "mealname") String mealName )
            throws MealNotFoundException {

        return  ResponseEntity.ok(mealService.updateMealByMealName(mealDTO, mealName));

    }

    @DeleteMapping("/delete")
    public void deleteMeal(@RequestParam(required = false, name="mealname") String mealName) {

        mealService.deleteMealByMealName(mealName);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteMealId(@PathVariable long id) {

        mealService.deleteMealById(id);
    }
}
