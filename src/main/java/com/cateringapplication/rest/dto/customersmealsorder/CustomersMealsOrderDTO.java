package com.cateringapplication.rest.dto.customersmealsorder;

import com.cateringapplication.model.Customer;
import com.cateringapplication.model.Meal;

import java.util.Date;

public class CustomersMealsOrderDTO {

    private Customer customer;

    private Meal meal;

    private Date date;

    public CustomersMealsOrderDTO() {
    }

    public CustomersMealsOrderDTO(Customer customer, Meal meal, Date date) {
        this.customer = customer;
        this.meal = meal;
        this.date = date;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Meal getMeal() {
        return meal;
    }

    public void setMeal(Meal meal) {
        this.meal = meal;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
