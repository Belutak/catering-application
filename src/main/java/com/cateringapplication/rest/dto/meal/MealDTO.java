package com.cateringapplication.rest.dto.meal;

import javax.validation.constraints.NotNull;

public class MealDTO {

    @NotNull
    private String mealName;

    public MealDTO() {
    }

    public MealDTO(String mealName) {
        this.mealName = mealName;
    }

    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }
}
