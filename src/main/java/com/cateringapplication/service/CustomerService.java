package com.cateringapplication.service;

import com.cateringapplication.exception.customer.CustomerNotFoundException;
import com.cateringapplication.model.Customer;
import com.cateringapplication.model.CustomersMeals;
import com.cateringapplication.rest.dto.customer.CustomerDTO;

import java.util.List;

public interface CustomerService {

    public CustomerDTO createCustomer(CustomerDTO customerDTO);

    public List<CustomerDTO> getAllCustomers() throws CustomerNotFoundException;

    public CustomerDTO getOneCustomerByEmail(String email) throws CustomerNotFoundException;

    public CustomerDTO updateCustomerByEmail(CustomerDTO customerDTO, String email) throws CustomerNotFoundException;

    public void deleteCustomerByEmail(String email) throws CustomerNotFoundException;

    public void deleteCustomerById(long id) throws CustomerNotFoundException;

    public CustomersMeals orderTesting(CustomerDTO customerDTO, String mealName);

}
