package com.cateringapplication.service;

import com.cateringapplication.exception.order.OrderNotFoundException;
import com.cateringapplication.model.CustomersMeals;
import com.cateringapplication.rest.dto.customer.CustomerDTO;
import com.cateringapplication.rest.dto.customersmealsorder.CustomersMealsOrderDTO;

import java.text.ParseException;
import java.util.List;

public interface CustomersMealsOrderService {

    public List<CustomersMealsOrderDTO> findAllOrders();

    public List<CustomersMealsOrderDTO> getAllOrdersByDate(String date) throws ParseException, OrderNotFoundException;


    public List<CustomersMeals> findByCustomerEmail(String email);
    public CustomersMealsOrderDTO createOrder(CustomerDTO customerDTO, String string);
}
