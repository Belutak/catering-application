package com.cateringapplication.service;

import com.cateringapplication.exception.meal.MealNotFoundException;
import com.cateringapplication.rest.dto.meal.MealDTO;

import java.util.List;

public interface MealService {

    public void createMeal(MealDTO mealDTO);

    public List<MealDTO> getAllMeals();

    public MealDTO getOneMealByMealName(String MealName);

    MealDTO updateMealByMealName(MealDTO mealDTO, String MealName) throws MealNotFoundException;

    public void deleteMealByMealName(String MealName);

    public void deleteMealById(long id);
}
