package com.cateringapplication.service.impl;

import com.cateringapplication.exception.customer.CustomerNotFoundException;
import com.cateringapplication.model.Customer;
import com.cateringapplication.model.CustomersMeals;
import com.cateringapplication.model.Meal;
import com.cateringapplication.repository.CustomerRepository;
import com.cateringapplication.repository.MealRepository;
import com.cateringapplication.repository.CustomersMealsOrderRepository;
import com.cateringapplication.rest.dto.customer.CustomerDTO;
import com.cateringapplication.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepo;

    @Autowired
    private MealRepository mealRepo;

    @Autowired
    private CustomersMealsOrderRepository orderRepo;


    @Override
    public CustomerDTO createCustomer(CustomerDTO customerDTO) {

        Customer tempCustomer = new Customer(customerDTO.getFirstName(), customerDTO.getLastName(), customerDTO.getEmail());
        customerRepo.save(tempCustomer);

        return customerDTO;
    }

    @Override
    public List<CustomerDTO> getAllCustomers() throws CustomerNotFoundException {

        List<Customer> customerList = (List) customerRepo.findAll();
        if (customerList.isEmpty()) {
            throw new CustomerNotFoundException("No customers were found!");
        }
        List<CustomerDTO> customerDTOlist = new ArrayList<>();
        for (Customer temp : customerList) {

            customerDTOlist.add(new CustomerDTO(temp.getFirstName(), temp.getLastName(), temp.getEmail()));
        }

        return customerDTOlist;
    }

    @Override
    public CustomerDTO getOneCustomerByEmail(String email) throws CustomerNotFoundException {

        Customer tempCustomer = customerRepo.findByEmail(email);
        if (tempCustomer == null) {
            throw new CustomerNotFoundException("No customer with email: " + email + " was found!");
        }

        return new CustomerDTO(tempCustomer.getFirstName(), tempCustomer.getLastName(), tempCustomer.getEmail());
    }

    @Override
    public CustomerDTO updateCustomerByEmail(CustomerDTO customerDTO, String email) throws CustomerNotFoundException {

        Customer tempCustomer = customerRepo.findByEmail(email);
        if (tempCustomer == null || customerDTO == null) {
            throw new CustomerNotFoundException("No customer with email: " + email + " was found!");
        }
        tempCustomer.setFirstName(customerDTO.getFirstName());
        tempCustomer.setLastName(customerDTO.getLastName());
        tempCustomer.setEmail(customerDTO.getEmail());

        return customerDTO;
    }

    @Override
    public void deleteCustomerByEmail(String email) throws CustomerNotFoundException {

        Customer tempCustomer = customerRepo.findByEmail(email);
        if (tempCustomer == null) {
            throw new CustomerNotFoundException("No customer with email: " + email + " was found!");
        }
        customerRepo.delete(tempCustomer);
    }

    @Override
    public void deleteCustomerById(long id) throws CustomerNotFoundException {

        Customer tempCustomer = customerRepo.findById(id).get();
        if (tempCustomer == null) {
            throw new CustomerNotFoundException("No customer with id: " + id + " was found!");
        }
        customerRepo.delete(tempCustomer);
    }

    @Override
    public CustomersMeals orderTesting(CustomerDTO customerDTO, String mealName) {

        Customer tempCustomer = new Customer(customerDTO.getFirstName(), customerDTO.getLastName(), customerDTO.getEmail());
        Meal tempMeal = mealRepo.findByMealName(mealName);
        Date creationDate = new Date();
        CustomersMeals customersMeals = new CustomersMeals(tempCustomer, tempMeal, creationDate);

        orderRepo.save(customersMeals);
        return customersMeals;
    }
}
