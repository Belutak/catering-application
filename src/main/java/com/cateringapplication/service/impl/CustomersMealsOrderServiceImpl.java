package com.cateringapplication.service.impl;

import com.cateringapplication.exception.order.OrderNotFoundException;
import com.cateringapplication.model.Customer;
import com.cateringapplication.model.CustomersMeals;
import com.cateringapplication.model.Meal;
import com.cateringapplication.repository.CustomerRepository;
import com.cateringapplication.repository.CustomersMealsOrderRepository;
import com.cateringapplication.repository.MealRepository;
import com.cateringapplication.rest.dto.customer.CustomerDTO;
import com.cateringapplication.rest.dto.customersmealsorder.CustomersMealsOrderDTO;
import com.cateringapplication.service.CustomersMealsOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class CustomersMealsOrderServiceImpl implements CustomersMealsOrderService {

    @Autowired
    private CustomersMealsOrderRepository orderRepo;

    @Autowired
    private CustomerRepository customerRepo;

    @Autowired
    private MealRepository mealRepo;


    //find all orders
    @Override
    public List<CustomersMealsOrderDTO> findAllOrders() {

        List<CustomersMeals> allOrders = (List) orderRepo.findAll();

        if (allOrders.isEmpty()) {
            new OrderNotFoundException("There were no previous orders!");
        }
        return  allOrders.stream()
                .map(m -> new CustomersMealsOrderDTO(m.getCustomer(), m.getMeal(), m.getCreationDate()))
                .collect(Collectors.toList());
    }

    //find all orders for a specific date
    @Override
    public List<CustomersMealsOrderDTO> getAllOrdersByDate(String date) throws ParseException, OrderNotFoundException {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date stringToDate = format.parse(date);
        List<CustomersMeals> orderList = orderRepo.findAllByCreationDate(stringToDate);

        if (orderList.isEmpty()) {
            throw new OrderNotFoundException("We could not find orders for that date");
        }
        List<CustomersMealsOrderDTO> orderDTOList = new ArrayList<>();

        orderList.forEach(p -> {
            orderDTOList.add(new CustomersMealsOrderDTO(p.getCustomer(), p.getMeal(), p.getCreationDate()));
        });

        return orderDTOList;
    }


    @Override
    public CustomersMealsOrderDTO createOrder(CustomerDTO customerDTO, String mealName) {

        Customer tempCustomer = new Customer(customerDTO.getFirstName(), customerDTO.getLastName(), customerDTO.getEmail());
        Meal tempMeal = mealRepo.findByMealName(mealName);
        CustomersMeals customersMeals = new CustomersMeals(tempCustomer, tempMeal, new Date());
        orderRepo.save(customersMeals);

        CustomersMealsOrderDTO tempOrderDTO = new CustomersMealsOrderDTO(customersMeals.getCustomer(), customersMeals.getMeal(), customersMeals.getCreationDate());
        return tempOrderDTO;

    }

    //test
    @Override
    public List<CustomersMeals> findByCustomerEmail(String email) {

        Customer tempCustomer = customerRepo.findByEmail(email);
        return tempCustomer.getCustomerMealList();

    }


}
