package com.cateringapplication.service.impl;

import com.cateringapplication.exception.meal.MealNotFoundException;
import com.cateringapplication.model.Meal;
import com.cateringapplication.repository.MealRepository;
import com.cateringapplication.rest.dto.meal.MealDTO;
import com.cateringapplication.service.MealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class MealServiceImpl implements MealService {

    @Autowired
    private MealRepository mealRepo;


    @Override
    public void createMeal(MealDTO mealDTO) {

        Meal tempMeal = new Meal(mealDTO.getMealName());
        mealRepo.save(tempMeal);
    }

    @Override
    public List<MealDTO> getAllMeals() {

        List<Meal> mealList = (List)mealRepo.findAll();
        List<MealDTO> mealDTOList = new ArrayList<>();

        for (Meal temp : mealList) {
            mealDTOList.add(new MealDTO(temp.getMealName()));
        }
        return mealDTOList;
    }

    @Override
    public MealDTO getOneMealByMealName(String mealName) {

        Meal tempMeal = mealRepo.findByMealName(mealName);
        return new MealDTO(tempMeal.getMealName());

    }

    @Override
    public MealDTO updateMealByMealName(MealDTO mealDTO, String mealName) throws MealNotFoundException {

        Meal tempMeal = mealRepo.findByMealName(mealName);

        if(tempMeal == null || mealDTO == null) {
            throw new MealNotFoundException("There is no such meal");
        }
        tempMeal.setMealName(mealDTO.getMealName());

        return mealDTO;
    }

    @Override
    public void deleteMealByMealName(String mealName) {

        Meal tempMeal = mealRepo.findByMealName(mealName);
        mealRepo.delete(tempMeal);
    }

    @Override
    public void deleteMealById(long id) {

        Meal tempMeal = mealRepo.findById(id).get();
        mealRepo.delete(tempMeal);
    }
}
