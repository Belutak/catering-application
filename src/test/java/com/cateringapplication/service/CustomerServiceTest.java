package com.cateringapplication.service;

import static org.junit.Assert.assertEquals;

import com.cateringapplication.exception.customer.CustomerNotFoundException;
import com.cateringapplication.model.Customer;
import com.cateringapplication.repository.CustomerRepository;
import com.cateringapplication.rest.dto.customer.CustomerDTO;
import com.cateringapplication.service.impl.CustomerServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceTest {

    @Mock
    private CustomerRepository customerRepo;

    @InjectMocks
    private CustomerServiceImpl customerService;

    private long id = 1;
    private CustomerDTO testCustomerDTO = new CustomerDTO("Michael", "Scott", "michael@gmail.com");
    private Customer testCustomer = new Customer("Michael", "Scott", "michael@gmail.com");
    private Customer testCustomer1 = new Customer("Michael", "Scott", "michael@gmail.com");
    private Customer testCustomer2 = new Customer("Dwight", "Schroute", "dwight@gmail.com");
    private List<CustomerDTO> testCustomerDTOList = new ArrayList<>();
    private List<Customer> testCustomerList = new ArrayList<>();
    private String email = "michael@gmail.com";

    @Test
    public void createCustomerTest() {

        customerService.createCustomer(testCustomerDTO);

        Mockito.verify(customerRepo, Mockito.times(1))
                .save(any(Customer.class));

    }

    @Test
    public void getAllCustomersTest() throws CustomerNotFoundException {
        //prepare
        testCustomerList.add(testCustomer);
        List<CustomerDTO> expected = new ArrayList<>();
        expected.add(testCustomerDTO);
        Mockito.when(customerRepo.findAll()).thenReturn(testCustomerList);

        //exec
        List<CustomerDTO> actual = customerService.getAllCustomers();

        //assert
        assertEquals(actual.get(0).getEmail(), expected.get(0).getEmail());
        assertEquals(actual.get(0).getFirstName(), expected.get(0).getFirstName());
        assertEquals(actual.get(0).getLastName(), expected.get(0).getLastName());
    }

    @Test
    public void getCustomerByEmailTest() throws CustomerNotFoundException {
        //prep
        CustomerDTO expected = testCustomerDTO;
        Mockito.when(customerRepo.findByEmail(email)).thenReturn(testCustomer);
        //exec
        CustomerDTO actual = customerService.getOneCustomerByEmail(email);
        //assert
        assertEquals(actual.getEmail(), expected.getEmail());
        assertEquals(actual.getLastName(), expected.getLastName());
        assertEquals(actual.getFirstName(), expected.getFirstName());
    }

    @Test
    public void deleteCustomerByEmailTest() throws CustomerNotFoundException {

        Mockito.when(customerRepo.findByEmail("michael@gmail.com")).thenReturn(testCustomer);

        customerService.deleteCustomerByEmail("michael@gmail.com");

        Mockito.verify(customerRepo, Mockito.times(1))
                .delete(testCustomer);
        //Mockito.verify(customerRepo, Mockito.times(1)).delete(any(Customer.class));   -- less complication this way
    }


//    @Override
////    public CustomerDTO updateCustomerByEmail(CustomerDTO customerDTO, String email) throws CustomerNotFoundException {
////
////        Customer tempCustomer = customerRepo.findByEmail(email);
////        if (tempCustomer == null || customerDTO == null) {
////            throw new CustomerNotFoundException("No customer with email: " + email + " was found!");
////        }
////        tempCustomer.setFirstName(customerDTO.getFirstName());
////        tempCustomer.setLastName(customerDTO.getLastName());
////        tempCustomer.setEmail(customerDTO.getEmail());
////
////        return customerDTO;
////    }

    @Test
    public void updateCustomerTest() {


    }

//    @Override
//    public void deleteCustomerById(long id) {
//
//        Customer tempCustomer = customerRepo.findById(id).get();
//        customerRepo.delete(tempCustomer);
//    }

//    @Test
//    public void deleteCustomerById() {
//
//        customerService.deleteCustomerById(id);
//
//        Mockito.verify(customerRepo, Mockito.times(1))
//                .deleteById(id);
//    }

//    @Override
//    public CustomerDTO getOneCustomerByEmail(String email) {
//
//        Customer tempCustomer = customerRepo.findByEmail(email);
//
//        return new CustomerDTO(tempCustomer.getFirstName(), tempCustomer.getLastName(), tempCustomer.getEmail());
//    }

}
