package com.cateringapplication.service;

import static org.junit.Assert.assertEquals;

import com.cateringapplication.exception.order.OrderNotFoundException;
import com.cateringapplication.model.Customer;
import com.cateringapplication.model.CustomersMeals;
import com.cateringapplication.model.Meal;
import com.cateringapplication.repository.CustomerRepository;
import com.cateringapplication.repository.CustomersMealsOrderRepository;
import com.cateringapplication.repository.MealRepository;
import com.cateringapplication.rest.dto.customersmealsorder.CustomersMealsOrderDTO;
import com.cateringapplication.service.impl.CustomersMealsOrderServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceTest {

    @Mock
    private CustomerRepository customerRepo;

    @Mock
    private CustomersMealsOrderRepository orderRepo;

    @Mock
    private MealRepository mealRepo;

    @InjectMocks
    private CustomersMealsOrderServiceImpl orderService;

    Date date;
    Customer tempCustomer = new Customer("Michael", "Scott", "micael@gmail.com");
    Meal tempMeal = new Meal("Burito");
    CustomersMealsOrderDTO orderDTO = new CustomersMealsOrderDTO(tempCustomer, tempMeal, date);
    CustomersMeals customersMeals = new CustomersMeals(tempCustomer, tempMeal, date);
    List<CustomersMeals> tempCustMeals = new ArrayList<>();



    @Test
    public void getOrdersByDateTest() throws ParseException, OrderNotFoundException {


        tempCustMeals.add(customersMeals);
        String date = "2018-10-10";
        List<CustomersMealsOrderDTO> expected = new ArrayList<>();
        expected.add(orderDTO);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date stringToDate = format.parse(date);
        Mockito.when(orderRepo.findAllByCreationDate(stringToDate)).thenReturn(tempCustMeals);

        List<CustomersMealsOrderDTO> actual = orderService.getAllOrdersByDate(date);

        assertEquals(actual.get(0).getCustomer().getFirstName(), expected.get(0).getCustomer().getFirstName());
        assertEquals(actual.get(0).getMeal().getMealName(), expected.get(0).getMeal().getMealName());
        assertEquals(actual.get(0).getDate(), expected.get(0).getDate());

    }



    @Test
    public void getAllOrders() {

        tempCustMeals.add(customersMeals);
        List<CustomersMealsOrderDTO> expected = new ArrayList<>();
        expected.add(orderDTO);

        Mockito.when(orderRepo.findAll()).thenReturn(tempCustMeals);

        List<CustomersMealsOrderDTO> actual = orderService.findAllOrders();

        assertEquals(actual.get(0).getCustomer().getFirstName(), expected.get(0).getCustomer().getFirstName());
        assertEquals(actual.get(0).getDate(), expected.get(0).getDate());
        assertEquals(actual.get(0).getMeal().getMealName(), expected.get(0).getMeal().getMealName());
    }


}
